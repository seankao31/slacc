class CreateChannels < ActiveRecord::Migration[5.0]
  def change
    create_table :channels, :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8' do |t|
      t.string :topic
      t.references :user, index: true, foreign_key: true
      t.timestamps
    end
  end
end
