class CreateUserChannelships < ActiveRecord::Migration[5.0]
  def change
    create_table :user_channelships, :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8' do |t|
      t.references :user, index: true, foreign_key: true
      t.references :channel, index: true, foreign_key: true
      t.integer :read, :default => 0
      t.timestamps
    end
  end
end
