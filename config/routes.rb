Rails.application.routes.draw do
  devise_for :users
  root 'welcome#index'

  resources :channels

  get 'channels/:id/join_channel', to: "channels#join_channel"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
